import os

import toml

from checker.utils import is_token_valid

PERSONAL_CONFIG_DICT = {}  # 记录个性化参数配置
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def get_config_info():
    config_file = os.path.join(BASE_DIR, "../config.toml")
    with open(config_file, "r", encoding="utf-8") as f:
        config_info = toml.load(f)
    return config_info


def set_config_info(info, file_dir):
    config_file = os.path.join(BASE_DIR, file_dir)
    with open(config_file, "a", encoding="utf-8") as f:
        toml.dump(info, f)


def get_access_token_from_config():
    config_info = get_config_info()
    return config_info['gitee_token']


def get_skip_mark_from_config(which_type):
    config_info = get_config_info()
    return config_info['skip'][which_type]


def get_feature_setting_from_config(which_type):
    config_info = get_config_info()
    return config_info['feature'][which_type]


def get_output_path_from_config():
    config_info = get_config_info()
    output_path = config_info['output_path']
    return output_path


def get_personal_config():
    config_info = get_config_info()
    feature_config_dict = dict(branch_or_tag=config_info['feature']['branch_or_tag'],
                               broken_link=config_info['feature']['find_broken_link'],
                               relative_path=config_info['feature']['find_broken_relative_path'],
                               closed_repo=config_info['feature']['find_closed_repo_link'],
                               missed_anchor=config_info['feature']['find_missed_anchor_point'],
                               just_all_link=config_info['feature']['just_find_all_link'],
                               url_to_skip=config_info['skip']['url_to_skip'],
                               keyword_to_skip=config_info['skip']['keyword_to_skip'],
                               postfix_to_skip=config_info['skip']['postfix_to_skip'],
                               prefix_to_skip=config_info['skip']['prefix_to_skip'])
    return feature_config_dict


def init_output_path():
    output_path = get_output_path_from_config()
    if output_path.lower() == 'default':
        output_path = os.path.join(BASE_DIR, '../output/')
    if not os.path.exists(output_path):
        os.makedirs(output_path, mode=0o777, exist_ok=False)
    return output_path


def init_repo_data_path_for_org(organization):
    output_path = init_output_path()
    clone_to_where = os.path.join(output_path, organization + '_repos/')
    if not os.path.exists(clone_to_where):
        os.makedirs(clone_to_where, mode=0o777, exist_ok=False)
    return clone_to_where


def check_config_is_valid():
    if PERSONAL_CONFIG_DICT['closed_repo']:
        access_token = get_access_token_from_config()
        assert is_token_valid(access_token), '检测关停仓链接需要有效的access_token，请于config.toml配置'


def init_global_config_setting():
    global PERSONAL_CONFIG_DICT
    if not PERSONAL_CONFIG_DICT:
        PERSONAL_CONFIG_DICT = get_personal_config()
    check_config_is_valid()
    return PERSONAL_CONFIG_DICT


if __name__ == '__main__':
    init_global_config_setting()
