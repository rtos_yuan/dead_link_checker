<!-- markdownlint-disable MD033 MD041-->

<p align="center">
  <h1 align="center">Dead Link Checker</h1>
</p>
<!-- markdownlint-disable MD033 MD041-->
<p align="center">
  <a href='https://gitee.com/rtos_yuan/dead_link_checker'><img src='https://gitee.com/rtos_yuan/dead_link_checker/widgets/widget_6.svg' alt='Fork me on Gitee'></img></a>
</p>

### 1、介绍

在开源社区，经常会遇到文档中链接无法访问的情况。这通常是由于发生了目录调整、文件重命名、删除，又没有及时更新引用这些发生变化的链接导致的。`Dead Link Checker`可以用于排查断链的情况，帮助代码仓维护人员识别断链，及时修复，避免给社区开发者带来不良的体验。

### 2、环境准备

Dead Link Checker属于Python脚本工具，需要安装Python最新版本。有关Python安装的信息，请访问[https://www.python.org/](https://www.python.org/)获取。

1. 克隆或下载脚本代码
   
    访问[https://gitee.com/rtos_yuan/dead_link_checker](https://gitee.com/rtos_yuan/dead_link_checker)进行克隆或者下载。

2. 安装Python三方包
   
   `Dead Link Checker`工具依赖一些Python三方包，在使用工具前，需要确保已经安装了依赖的Python三方包。如果没有安装，在`dead_link_checker`目录，进入命令行终端，执行`pip install -r requirements.txt`需要安装依赖的三方包，只需要执行一次，如果已经安装依赖的三方包，则不需要执行。 还可以直接执行如下命令：
   
   ```shell
   pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple requests
   pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple pandas
   pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple toml
   pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple xToolkit
   pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple xlrd
   ```

### 3、使用说明

支持多种方排查文档无效链接。如果指定代码仓URL，或者排查Gitee开源社区特定组织下的代码仓下的无效链接情况，使用该工具需要提前在运行环境中安装配置好git命令行工具。

#### 3.1 检查文档URL中的断链

  ```shell
  python check.py -u <url_need_to_check>
  ```

  结果默认生成于 `output/日期+_md链接_断链排查结果报告.xlsx`

#### 3.2 检查本地文档文件中的断链

  ```shell
  python check.py -f <file_path_need_to_check>
  ```

  结果默认生成于 `output/日期+_本地文件_断链排查结果报告.xlsx`

#### 3.3 检查本地仓库中所有文档文件中的断链

  ```shell
  python check.py -d <dir_path_need_to_check>
  ```

  结果默认生成于 `output/日期+_本地仓库_断链排查结果报告.xlsx`

#### 3.4 传入仓库URL，自动clone，并排查断链

  ```shell
  python check.py -r <repo_url_need_to_check>
  ```

  结果默认生成于 `output/日期+_链接仓库_断链排查结果报告.xlsx`

  仓库默认拉取至 `output目录`

#### 3.5 传入Gitee社区组织名，自动clone，排查组织断链

  - 配置token信息于工具根目录下的`config.toml`文件 
    
    访问[Gitee OpenAPI](https://gitee.com/api/v5/swagger)，点击右上角的申请授权按钮申请，把申请到的`access_token`替代文件中的"Please input your own token"，需要保留双引号。
    
    ```toml
    gitee_token= "Please input your own token"
    ```
  
  - 执行命令
    
    ```shell
    python check.py -o <organization_need_to_check> 
    ```

  结果默认生成于 `output/日期+_组织仓库_断链排查结果报告.xlsx`

  仓库默认拉取至 `output目录`

### 4、 进阶配置

#### 4.1 仓库检出与结果报告输出目录的选择

配置目录信息，于工具根目录下的`config.toml`文件 

将目录信息，替换文件中的"default"，需要保留双引号。

```toml
output_path = "default"
```

#### 4.2 仓库检出的branch或tag的选择

对于涉及检出clone的功能，可以通过传参，选择需要排查的分支或标签

```shell
python check.py -r <repo_url_need_to_check> --branch=<branch_or_tag>
```

```shell
python check.py -o <organization_need_to_check> --branch=<branch_or_tag>
```

#### 4.3 所排查的目标链的类型

对于[使用说明](#3使用说明)中所提及方法，都默认只检查目标项中的断链和失效相对路径。

工具还支持对目标项中**涉及组织关停仓的链接**、**失效锚点**的排查，以及对目标项中**所有链接**的罗列

 **单次检测配置** 

如果只是想获知，目标项中，涉及了哪些链接，而不作任何筛选

- 在[使用说明](#3使用说明)中所提及方法末尾，添加参数项`--just_all_link`

  ```shell
  python check.py -o <organization_need_to_check> --just_all_link
  ```

如果在检查断链的同时，还想添加其他排查项，可以在[使用说明](#3使用说明)中所提及方法末尾，组合传入下列目标链参数

- 检测涉及组织关停仓的链接


  ```shell
  python check.py -o <organization_need_to_check> --closed_repo
  ```
- 失效锚点


  ```shell
  python check.py -o <organization_need_to_check> --missed_anchor
  ```

  > 组合使用示例：排查 断链 + 失效相对路径 + 关停仓链接 +  失效锚点
  >
  > ```shell
  > python check.py -o <organization_need_to_check> --closed_repo --missed_anchor
  > ```
  >
  > 备注：`--just_all_link`不可组合使用，会覆盖其他筛选需求

**固定检测配置**

如果不想每次传参，而是固定检测特定组合的目标链类型，可于工具的根目录下`config.toml`文件进行配置 

```toml
# Config the feature of the checker
[feature]
branch_or_tag = "master" # 检测仓库链接时，检查哪一个分支
find_broken_link = true  # 是否检测断链
find_broken_relative_path = true  # 是否检测失效相对路径
find_closed_repo_link = false  # 是否检测关停仓链接
find_missed_anchor_point = false  # 是否检测失效锚点
just_find_all_link = false  # 是否探测返回所有链接
```
<!-- panels:start -->

<!-- div:title-panel -->
#### 4.4 跳过排查的白名单链接
<!-- div:left-panel -->
对于无需检测的链接，或含关键词的特征链接，可以通过配置`config.toml`文件 ，进行跳过

<!-- div:right-panel -->

```toml
# Config the url or keyword no need to check
[skip]
# 对于此链接，进行跳过
url_to_skip = [
 'https://repo.harmonyos.com/ohpm', 
]
# 对于含有如下关键词的链接，进行跳过
keyword_to_skip = [ 
 'ci.openharmony',
]
# 对于如下关键词开头的链接，进行跳过
prefix_to_skip = [
 'http://127.0.0.1',
]
# 对于如下关键词结尾的链接，进行跳过
postfix_to_skip = [
 'XXX',
]

```
<!-- panels:end -->
### 5、报告说明

下面以[Gitee社区组织名链接排查](#35-传入gitee社区组织名自动clone排查组织断链)的结果报告为例，予以说明。其他类型报告，皆以此为基础，作列的删减。

![image-20230511153946191](./pic/README/image-20230511153946191.png)

- 仓库名称

  对于涉及检出的功能，记录该条目标链，所属的检出仓库的名称

- 文档名称

  对于涉及检出的功能，记录该条目标链，所属文档的路径与名称

- 文档链接-raw

  对于涉及检出的功能，记录该条目标链，所属文档的原始文件url，方便检索具体位置

- 文档中目标链

  记录具体目标链的内容

- 文档链接-blob

  对于涉及检出的功能，记录该条目标链，所属文档的Gitee文件url，方便查看响应效果

- 目标链类型

  记录该条目标链所属类型，取值可能如下：

  - 关于链接排查：断链、失效相对路径、关停仓链接、失效锚点

  - 关于返回全部连接：链接、相对路径、锚点

### 6、参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

### 7、友情链接

[OpenHarmony应用开发范例](https://rtos_yuan.gitee.io/oh_app)

[知识分享站点导航](https://rtos_yuan.gitee.io)