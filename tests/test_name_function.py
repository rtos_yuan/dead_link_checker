import os
import unittest

from checker.dead_link_check import is_url_deadlink, is_relative_path_broken
from checker.utils_str import form_absolute_url_path


class DeadLinksCheckTestCase(unittest.TestCase):
    """测试 dead_link_check.py"""

    def test_is_url_deadlink(self):
        """判断是否能够正确判断某url是否是死链"""
        live_url = 'https://www.huawei.com'
        dead_url = 'https://www.huawei.com/testdeadlinks'
        self.assertFalse(is_url_deadlink(live_url))
        self.assertTrue(is_url_deadlink(dead_url))

    def test_is_path_broken(self):
        """判断是否能够正确判断某相对路径是否是死链"""
        absolute_path = os.path.abspath(__file__)
        live_relative_path = '../checker/dead_link_check.py'
        broken_relative_path = '../checker/dead_link_checkNOT.py'
        check_local = True
        self.assertTrue(is_relative_path_broken(broken_relative_path, absolute_path, check_local))
        self.assertFalse(is_relative_path_broken(live_relative_path, absolute_path, check_local))


class UtilsTestCase(unittest.TestCase):
    """测试 utils/utils_str.py"""

    def test_relative_url_to_abs_url(self):
        """判断是否将相对路径合成正确的绝对路径"""
        absolute_path = 'https://gitee.com/openharmony/docs/readme.md'
        relative_path = '../application-dev/common-event.md'
        result_path = 'https://gitee.com/openharmony/application-dev/common-event.md'
        self.assertEqual(form_absolute_url_path(relative_path, absolute_path), result_path)
