#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
from checker.terminal_interface import launch_checker


def main(argv):
    launch_checker(argv)


if __name__ == '__main__':
    main(sys.argv[1:])
