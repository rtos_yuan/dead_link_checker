<h2 id="Dead Link Checker简介">Dead Link Checker简介</h2>

Dead Link Checker可以用于排查社区文档断链的情况，帮助代码仓维护人员快速识别、修复断链，给社区开发者带来更好的体验。

<h2>Dead Link Checker 使用手册</h2>

请访问[Dead Link Checker 使用手册](https://rtos_yuan.gitee.io/dead_link_checker/#/)，获取软件安装、环境配置、使用示例等等。

<h2 id="在线反馈">在线反馈</h2>

感谢您使用Dead Link Checker工具，如遇到任何问题或有任何建议，请随时向我们[反馈](https://gitee.com/rtos_yuan/dead_link_checker/issues)，我们将帮助您解决。
